package com.icsug;

import java.util.ArrayList;

public class Agenda {
	public static ArrayList<AgendaBullet> getAgenda() {
		ArrayList<AgendaBullet> agenda = new ArrayList<AgendaBullet>();
		agenda.add(new AgendaBullet("Application Config Re-Invented", "appconfig.xsp"));
		agenda.add(new AgendaBullet("Smart User Config", "userconfig.xsp"));
		agenda.add(new AgendaBullet("Facetted Search in Domino", "search.xsp"));
		return agenda;
	}
}
