package com.icsug;

public class AgendaBullet {

	private final String title;
	private final String url;
	
	public AgendaBullet(final String title, final String url) {
		this.title = title;
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public String getUrl() {
		return url;
	}

}
