package com.icsug;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.context.FacesContext;

import org.openntf.domino.Document;
import org.openntf.domino.DocumentCollection;
import org.openntf.domino.utils.XSPUtil;

import com.ibm.xsp.designer.context.XSPContext;

public class AppConfigBean implements Serializable {

	private static final long serialVersionUID = 1815182743940537568L;

	private Map<String, ArrayList<String>> valueLists = null;
	private ResourceBundle bundle = null;
	private String appEvent = "";
	private String appEventDate = "";
	private String appConferenceLogo = "/icons/ecblank.gif";

	public AppConfigBean() {
		init();
	}

	@SuppressWarnings("unchecked")
	private void init() {
		valueLists = new HashMap<String, ArrayList<String>>();
		DocumentCollection col = XSPUtil.getCurrentDatabase().getView("valuelists").getAllDocuments();
		for (Document doc : col) {
			ArrayList<String> list = new ArrayList<String>();
			list = doc.getItemValue("values", ArrayList.class);
			valueLists.put(doc.getItemValueString("key"), list);
		}

		// app properties
		try {
			XSPContext xspContext = XSPContext.getXSPContext(FacesContext.getCurrentInstance());
			bundle = xspContext.bundle("app");
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (bundle != null) {
			appEvent = bundle.getString("appEvent");
			appEventDate = bundle.getString("appEventDate");
			appConferenceLogo = bundle.getString("appConferenceLogo");
		}
	}

	public void reload() {
		init();
	}

	public ArrayList<String> getList(final String key) {
		return valueLists.get(key);
	}

	public ArrayList<String> getSortedList(final String key) {
		ArrayList<String> list = valueLists.get(key);
		if (list != null) {
			Collections.sort(list);
		}
		return list;
	}

	public String getAppTitle() {
		return appEvent;
	}

	public String getAppEventDate() {
		return appEventDate;
	}

	public String getConferenceLogo() {
		return appConferenceLogo;
	}

	/**
	 * Read the app icon
	 * 
	 * @return
	 */
	public String getAppIcon() {
		try {
			return valueLists.get("appIcon").get(0);
		} catch (Exception e) {
			return "fa fa-coffee";
		}
	}

	public String getString(final String key) {
		try {
			return valueLists.get(key).get(0);
		} catch (Exception e) {
			return null;
		}

	}

	public static boolean isAdmin() {
		return XSPUtil.getCurrentDatabase().queryAccessRoles(XSPUtil.getCurrentSession().getEffectiveUserName()).contains("[Admin]");
	}

}
