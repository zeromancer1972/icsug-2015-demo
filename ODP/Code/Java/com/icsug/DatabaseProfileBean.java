/**
 * @author Oliver Busse - http://www.oliverbusse.com - @zeromancer1972
 */
package com.icsug;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.openntf.domino.Document;
import org.openntf.domino.Item;
import org.openntf.domino.utils.XSPUtil;

public class DatabaseProfileBean implements Serializable {

	private static final long serialVersionUID = 1L;

	// class member
	private Map<String, ArrayList<Object>> valueList = null;
	private String unid;

	// bean field example
	private final String someGlobalString = "I am a global string value right from the DB profile, available all around the XSP context :-)";

	/**
	 * Bean constructor
	 */
	public DatabaseProfileBean() {
		init();
	}

	/**
	 * This will be called when the application is loaded AND whenever you have
	 * to reload changes to your XPages context. Rename the view, the key and the
	 * form name if you like
	 */
	@SuppressWarnings({ "deprecation", "unchecked" })
	public void init() {
		// init the list
		valueList = new HashMap<String, ArrayList<Object>>();

		// try to get the database profile document from the lookup view
		Document profile;
		profile = XSPUtil.getCurrentDatabase().getView("profiles").getDocumentByKey("dbprofile", true);

		// if the document isn't there, one is created
		if (profile == null) {
			profile = XSPUtil.getCurrentDatabase().createDocument();
			profile.replaceItemValue("Form", "frmDatabaseProfile");
			profile.replaceItemValue("key", "dbprofile");
			// set other default field values etc.
			profile.save(true, false); // cheating: OpenNTF API catches the
			// exception if user cannot create/save
			// data
		}

		// we don't care if there are no items inside

		for (Item it : profile.getItems()) {
			// we could or should pass system fields...
			ArrayList<Object> list = new ArrayList<Object>();
			list = profile.getItemValue(it.getName(), ArrayList.class);
			// maybe we can suppress size-0 lists here...
			/*
			 * if(list.size()==0) { list.add(""); }
			 */
			valueList.put(it.getName(), list);
		}
		
		// store the UNID, too
		this.unid = profile.getUniversalID();
	}

	/**
	 * Simple get method for XSP usage. Don't forget to use get(0) for a single
	 * value as the returned value is always an ArrayList. Just like you know
	 * from getItemValue(...)(0) :-)
	 * 
	 * @param itemName
	 * @return
	 */
	public Object get(final String itemName) {
		return this.valueList.get(itemName);
	}

	/**
	 * Return the whole list (this is for the demo page only)
	 * 
	 * @return
	 */
	public Map<String, ArrayList<Object>> getValueList() {
		return this.valueList;
	}

	/**
	 * This one is just an example for a somewhat "global" field that can be
	 * used everywhere in the XSP context
	 * 
	 * @return
	 */
	public String getSomeGlobalString() {
		return someGlobalString;
	}

	/**
	 * Get the UNID of the profile for e.g. editing
	 * @return
	 */
	public String getUnid() {
		return unid;
	}
}
