/**
 * @author Oliver Busse - http://www.oliverbusse.com - @zeromancer1972
 */
package com.icsug;

import java.io.Serializable;
import java.util.Vector;

import lotus.domino.Database;

import org.openntf.domino.Name;
import org.openntf.domino.utils.XSPUtil;

public class EnvironmentBean implements Serializable {

	private static final long serialVersionUID = -9101361786057152356L;

	private int aclLevel;
	private int aclOptions;
	private String userName;
	private String commonUserName;
	private String abbreviatedUserName;
	private Vector<String> userRoles;

	public EnvironmentBean() {
		try {
			this.userName = XSPUtil.getCurrentSession().getEffectiveUserName();
			Name n = XSPUtil.getCurrentSession().createName(userName);
			this.commonUserName = n.getCommon();
			this.abbreviatedUserName = n.getAbbreviated();

			this.userRoles = XSPUtil.getCurrentDatabase().getACL().getRoles();

			this.aclLevel = XSPUtil.getCurrentDatabase().queryAccess(this.userName);
			this.aclOptions = XSPUtil.getCurrentDatabase().queryAccessPrivileges(this.userName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Can the current user create new documents?
	 * 
	 * @return
	 */
	public boolean isCreateDocuments() {
		return (this.aclOptions & Database.DBACL_CREATE_DOCS) > 0;
	}

	/**
	 * Can the current user delete documents?
	 * 
	 * @return
	 */
	public boolean isDeleteDocuments() {
		return (this.aclOptions & Database.DBACL_DELETE_DOCS) > 0;
	}

	/**
	 * ACl level for current user
	 * 
	 * @return
	 */
	public synchronized int getAclLevel() {
		return aclLevel;
	}

	/**
	 * ACL options for current user
	 * 
	 * @return
	 */
	public synchronized int getAclOptions() {
		return aclOptions;
	}

	/**
	 * Username variations
	 * 
	 * @return
	 */
	public synchronized String getUserName() {
		return userName;
	}

	public String getCommonUserName() {
		return commonUserName;
	}

	public String getAbbreviatedUserName() {
		return abbreviatedUserName;
	}

	/**
	 * Get all user roles
	 * 
	 * @return
	 */
	public Vector<String> getUserRoles() {
		return userRoles;
	}

	/**
	 * Get any environment string value by key
	 * Only system entries can be read from the server's notes.ini
	 * @param name
	 * @return
	 */
	public String getEnvironmentString(final String name) {
		try {
			return XSPUtil.getCurrentSession().getEnvironmentString(name, true);
		} catch (Exception e) {
			return "";
		}
	}
}
