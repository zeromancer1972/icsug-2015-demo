/**
 * @author Oliver Busse - http://www.oliverbusse.com - @zeromancer1972
 */
package com.icsug;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.openntf.domino.Database;
import org.openntf.domino.Document;
import org.openntf.domino.DocumentCollection;
import org.openntf.domino.utils.XSPUtil;

import com.ibm.xsp.extlib.util.ExtLibUtil;

public class FacettedSearchBean implements Serializable {

	private static final long serialVersionUID = -7607625072147178477L;
	private boolean isFTIndexed;
	private List<String> facetlist = null;
	private ArrayList<SearchResultEntry> result = null;
	private String search;
	private ArrayList<String> facets;
	private String searchFormula;
	private String dbName;
	private String resultTextFormula;

	public FacettedSearchBean() {
		init();
	}

	/**
	 * Start method to do a search
	 */
	@SuppressWarnings("unchecked")
	public void init() {

		result = new ArrayList<SearchResultEntry>();
		facetlist = new ArrayList<String>();

		try {
			AppConfigBean application = (AppConfigBean) ExtLibUtil.resolveVariable(FacesContext.getCurrentInstance(), "application");
			this.searchFormula = application.getList("search").get(0);
			this.dbName = application.getList("sourcedb").get(0);
			this.resultTextFormula = application.getList("resulttext").get(0);

			try {
				// set the form of the source documents
				search = ExtLibUtil.getSessionScope().get("searchTerm").toString();
			} catch (Exception e) {
				search = "";
			}

			try {
				facets = (ArrayList) ExtLibUtil.getSessionScope().get("searchFacets");
			} catch (Exception e) {
				facets = null;
			}

			try {
				Database db = XSPUtil.getCurrentSession().getDatabase(XSPUtil.getCurrentDatabase().getServer(), this.dbName);
				// is FT indexed?
				this.isFTIndexed = db.isFTIndexed();
				// simple search first
				DocumentCollection col = db.search(searchFormula);
				if (!search.equals("")) {
					// a search term was provided, do a normal FT search in the collection
					col.FTSearch((search.indexOf("*") == -1 ? "*" + search + "*" : search), 100);
				}

				if (facets != null) {
					try {
						// implode the facets
						String facetString = "";
						for (String f : facets) {
							facetString += f + " OR ";
						}
						// cut last OR
						facetString = facetString.substring(0, facetString.length() - 4);
						// FT search with facets provided
						col.FTSearch(facetString, 100);
					} catch (Exception e) {
						// catch the empty array list
					}
				}

				for (Document doc : col) {
					// store link text data dependent from the document type
					result.add(new SearchResultEntry(doc.getUniversalID(), this.getLinkText(doc)));
					List tags = doc.getItemValue("postTags", ArrayList.class);
					for (int x = 0; x < tags.size(); x++) {
						try {
							facetlist.add(tags.get(x).toString().toLowerCase());
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Reset search term (via button)
	 */
	public void resetSearch() {
		ExtLibUtil.getSessionScope().remove("searchTerm");
		ExtLibUtil.getSessionScope().remove("searchFacets");
		init();
	}

	/**
	 * Reset facets (via button)
	 */
	public void resetFacets() {
		ExtLibUtil.getSessionScope().remove("searchFacets");
		init();
	}

	/**
	 * Get results
	 * 
	 * @return
	 */
	public ArrayList<SearchResultEntry> getResults() {
		return result;
	}

	/**
	 * Compose link text to corresponding document
	 * 
	 * @param doc
	 * @return
	 */
	private String getLinkText(final Document doc) {
		return XSPUtil.getCurrentSession().evaluate(this.resultTextFormula, doc).elementAt(0).toString();
	}

	public boolean isFTIndexed() {
		return isFTIndexed;
	}

	public List<SelectItem> getFacets() {
		// return a sorted and unique list
		if (facetlist != null) {
			Set<String> treeset = new TreeSet<String>(facetlist);
			List<SelectItem> items = new ArrayList<SelectItem>();
			for (String facet : treeset) {
				items.add(new SelectItem(facet, facet));
			}
			return items;
		} else {
			return null;
		}

	}

	public String getSearch() {
		return search;
	}

}
