package com.icsug;

public class SearchResultEntry {
	
	private String unid;
	private String linkText;

	public SearchResultEntry(final String unid, final String linkText) {
		this.unid = unid;
		this.linkText = linkText;
	}

	public String getUnid() {
		return unid;
	}

	public void setUnid(final String unid) {
		this.unid = unid;
	}

	public String getLinkText() {
		return linkText;
	}

	public void setLinkText(final String linkText) {
		this.linkText = linkText;
	}
	
	
}
