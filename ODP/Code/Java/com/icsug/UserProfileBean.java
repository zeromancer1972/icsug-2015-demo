/**
 * @author Oliver Busse - http://www.oliverbusse.com - @zeromancer1972
 */

package com.icsug;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.openntf.domino.Document;
import org.openntf.domino.Item;
import org.openntf.domino.utils.XSPUtil;

public class UserProfileBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Map<String, ArrayList<Object>> valueList = null;
	private String unid = null;

	/**
	 * Bean constructor
	 */
	public UserProfileBean() {
		init();
	}

	/**
	 * This will be called when the HTTP session is generated for the user
	 */
	@SuppressWarnings({ "deprecation", "unchecked" })
	public void init() {
		// init the list
		valueList = new HashMap<String, ArrayList<Object>>();

		// the lookup key is the username
		String key = XSPUtil.getCurrentSession().getEffectiveUserName();

		// try to get the database profile document from the lookup view
		Document profile;
		profile = XSPUtil.getCurrentDatabase().getView("profiles").getDocumentByKey(key, true);

		// if the document isn't there, one is created
		if (profile != null) {
			unid = profile.getUniversalID();
			// we don't care if there are no items inside
			for (Item it : profile.getItems()) {
				ArrayList<Object> list = new ArrayList<Object>();
				list = profile.getItemValue(it.getName(), ArrayList.class);
				valueList.put(it.getName(), list);
			}
		}
	}

	public String getUnid() {
		return this.unid;
	}

	/**
	 * Simple get method for XSP usage
	 * 
	 * @param itemName
	 * @return
	 */
	public ArrayList<Object> get(final String itemName) {
		return this.valueList.get(itemName);
	}

	/**
	 * Return the whole list (this is for the demo page only)
	 * 
	 * @return
	 */
	public Map<String, ArrayList<Object>> getValueList() {
		return this.valueList;
	}
}
