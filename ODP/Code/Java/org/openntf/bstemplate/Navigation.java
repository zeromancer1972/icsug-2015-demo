package org.openntf.bstemplate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.openntf.domino.Name;
import org.openntf.domino.utils.XSPUtil;

import com.icsug.AppConfigBean;
import com.icsug.UserProfileBean;

public class Navigation implements Serializable {

	private static final long serialVersionUID = 8031965383531253276L;
	private final List<Page> navigation;

	@SuppressWarnings("deprecation")
	public Navigation() {
		this.navigation = new ArrayList<Page>();
		this.navigation.add(new Page("Database Profile", "dbprofile.xsp", "fa fa-wrench", false, AppConfigBean.isAdmin()));
		this.navigation.add(new Page("Value Lists", "valuelists.xsp", "fa fa-th-list", false, AppConfigBean.isAdmin()));
		this.navigation.add(new Page("Environment", "env.xsp", "fa fa-dashboard", false));
		this.navigation.add(new Page("All Documents", "all.xsp", "fa fa-list", false));
		this.navigation.add(new Page("Search", "search.xsp", "fa fa-search", false, AppConfigBean.isAdmin()));		

		if (XSPUtil.getCurrentSession().getEffectiveUserName().equals("Anonymous")) {
			this.navigation.add(new Page("Login", "login.xsp", "fa fa-key", false));
		} else {
			Name un = XSPUtil.getCurrentSession().createName(XSPUtil.getCurrentSession().getEffectiveUserName());
			String user = un.getCommon();
			try {
				UserProfileBean userprofile = (UserProfileBean) XSPUtil.resolveVariable("userprofile");
				user = userprofile.get("userNickname").get(0).toString();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			this.navigation.add(new Page("Welcome, " + user, "userprofileDoc.xsp", "fa fa-user", false));
		}
	}

	public List<Page> getNavigation() {
		return navigation;
	}
}
