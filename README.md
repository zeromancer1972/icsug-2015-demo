# ICS.UG/Engage.UG 2015 Demo Application #
This is the live demo application from the session at ICS.UG and Engage.UG in 2015

Please be aware of the application to use several plugins to run on your server:

* OpenNTF Domino API
* Bootstrap4XPages
* Extension Library from OpenNTF (latest)